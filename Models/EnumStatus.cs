namespace tech_test_payment_api.Models
{
    public enum EnumStatus
    {
        Aguardando_pagamento,
        Pagamento_aprovado,
        Enviado_para_transportadora,
        Entregue,
        Cancelada
        
    }
}