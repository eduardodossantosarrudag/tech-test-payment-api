using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda)
        {

            if(venda.Produtos.Count == 0)
                return BadRequest(new { Erro = "A Lista de produtos deve possuir pelo menos 1 item" });

            foreach( var produto in venda.Produtos)
                if(produto.quantidade <= 0)
                    return BadRequest(new { Erro = "A quantidade não pode ser zero" });

            venda.Status = EnumStatus.Aguardando_pagamento;

            _context.Venda.Add(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(BuscarVenda), new { id = venda.Id}, venda);
        } 

        [HttpGet("{id}")]
        public IActionResult BuscarVenda(int id)
        {
            var venda = _context.Venda.Find(id);
           
            if(venda == null)
                return NotFound();

            venda.Vendedor = _context.Vendedor.Where(x => x.VendaId == venda.Id).First<Vendedor>();

            venda.Produtos = _context.Produto.Where(x => x.VendaId == venda.Id).ToList<Produto>();
            
            return Ok(venda);
        }

        [HttpPut("{id}")]
        public IActionResult AtualizarStatusVenda(int id, EnumStatus status){

            var venda = _context.Venda.Find(id);
           
            if(venda == null)
                return NotFound();

            if (venda.Status == EnumStatus.Aguardando_pagamento && status == EnumStatus.Pagamento_aprovado)
            {
                venda.Status = status;
            }
            else if(venda.Status == EnumStatus.Aguardando_pagamento && status == EnumStatus.Cancelada)
            {
                venda.Status = status;
            }
            else if(venda.Status == EnumStatus.Pagamento_aprovado && status == EnumStatus.Enviado_para_transportadora)
            {
                venda.Status = status;
            }
            else if(venda.Status == EnumStatus.Pagamento_aprovado && status == EnumStatus.Cancelada)
            {
                venda.Status = status;
            }
            else if(venda.Status == EnumStatus.Enviado_para_transportadora && status == EnumStatus.Entregue)
            {
                venda.Status = status;
            }
            else{
                return BadRequest(new { Erro = "O status da venda não pode ser atualizado para este status" });
            }

            _context.Venda.Update(venda);
            _context.SaveChanges();

            return Ok(venda);
        }
    }
}